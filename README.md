# First App

### Keynotes
  - Intro to JS
  - Intro to Node.js & NPM
  - Intro to Express.js
  - Nodemon
  - Create first web server using express.js
  - How to write URL and how to pass parameters

### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ cd first-app
$ npm i
$ nodemon index.js
```



By **Russul H.**

At **3/1/2019**