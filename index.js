const express = require('express')
const app = express()
const port = 5000

app.get('/', (req, res) => res.send('Hello World!'))
app.get('/name/:id',(req, res)=>{
   // console.log(req);
   res.send("your name is "+req.params.id)
})

app.get('*', (req, res)=>{
   res.status(404).send('Not found');
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))